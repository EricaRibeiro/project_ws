package pt.isep.clinic.model;
import pt.isep.clinic.exception.*;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Gestao implements Serializable {
    private List<Clinica> clinicas = new ArrayList<>();

    public List<Clinica> getClinicas() {
        return clinicas;
    }

    public void setClinicas(List<Clinica> clinicas) {
        this.clinicas = clinicas;
    }

    public int getPosicaoClinicaByNif(long nif){
        for(int i = 0; i < clinicas.size(); i++){
            if(clinicas.get(i).getNif() == nif)
                return i;
        }
        return -1;
    }

    public Clinica getClinicaByNif(long nif){
        for(int i = 0; i < clinicas.size(); i++){
            if(clinicas.get(i).getNif() == nif)
                return clinicas.get(i);
        }
        return null;
    }

    public void adicionarPessoa(Pessoa p, int posicaoInstituicao){
       for(int i = 0; i < clinicas.size(); i++){
            for(int j = 0; j < clinicas.get(i).getPessoas().size(); j++){
                if(p.getCartaoCidadao() == clinicas.get(i).getPessoas().get(j).getCartaoCidadao() && posicaoInstituicao != getPosicaoClinicaByNif(clinicas.get(i).getNif()))
                    throw new NifDuplicadoException("A pessoa já se encontra registada noutra clínica!");
                else if(p instanceof Dentista && clinicas.get(i).getPessoas().get(j) instanceof Dentista && ((Dentista) p).getNumeroCedula() == ((Dentista) clinicas.get(i).getPessoas().get(j)).getNumeroCedula() && posicaoInstituicao != getPosicaoClinicaByNif(clinicas.get(i).getNif()))
                    throw new NumeroCedulaDuplicadoException("O dentista já se encontra registado noutra clínica!");
            }
        }
        clinicas.get(posicaoInstituicao).addPessoa(p);
    }

    public void eliminarPessoa(long cartaoCidadao, int posicaoClinica) {
        for(int i = 0; i < clinicas.get(posicaoClinica).getServicos().size(); i++){
            if(clinicas.get(posicaoClinica).getServicos().get(i).getIdentificacaoCliente() == cartaoCidadao)
                eliminarConsulta(clinicas.get(posicaoClinica).getServicos().get(i).getCodigoConsulta(), clinicas.get(posicaoClinica).getNif());
        }
        clinicas.get(posicaoClinica).deletePessoa(cartaoCidadao);
    }

    public void adicionarClinica(Clinica c){
        for(int i = 0; i < clinicas.size(); i++){
            if(clinicas.get(i).getNif() == c.getNif()){
                throw new NifDuplicadoException("Já existe uma clínica registada com o NIF introduzido!");
            }
            else if(!Validacoes.validarTamanhoNumeros(c.getNif()))
                return;
        }
        clinicas.add(c);
    }

    public void eliminarClinica(long nif){
        if(getPosicaoClinicaByNif(nif) != -1){
            for(int i = 0; i < clinicas.size(); i++){
                if(nif == clinicas.get(i).getNif()){
                    Clinica c = clinicas.get(i);
                    clinicas.remove(c);
                }
            }
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public void adicionarConsulta(Servico s, long nif) throws ParseException {
        if(!s.getHoraRealizacao().eAnterior(Hora.obterHoraAtual().getHora(), Hora.obterHoraAtual().getMinutos(), s.getDataRealizacao()))
            throw new HoraInvalidaException("A hora introduzida não é válida!");
        else {
            if(!Validacoes.validarTipoServico(s.getTipoServico()))
                throw new ServicoInexistenteException("A clínica não oferece o serviço introduzido!");
            else {
                if(!Hora.validarHora(s.getHoraRealizacao().getHora(), s) || !Hora.validarMinutos(s.getHoraRealizacao().getMinutos()))
                    return;
                else if(!Validacoes.validarCodigoConsulta(s.getCodigoConsulta()))
                    throw new CodigoConsultaInvalidoException("O código de consulta deve ser composto por 4 dígitos!");
                }
        }
        Clinica c = getClinicaByNif(nif);
        c.adicionarConsulta(s);
    }

    public void eliminarConsulta(int codigoConsulta, long nif){
        Clinica c = getClinicaByNif(nif);
        c.eliminarConsulta(codigoConsulta);
    }

    public Clinica buscarClinicaMaisUrgencias(){
        int[] count = new int[clinicas.size()];
        for(int i = 0; i < count.length; i++){
            List<Servico> servicos = clinicas.get(i).getServicos();
            count[i] = 0;
            for(int j = 0; j < servicos.size(); j++){
                if(servicos.get(j).getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("urgência"))
                    count[i] += 1;
            }
        }
        for(int h = 0; h < count.length - 1; h++){
            for(int a = h + 1; a < count.length; a++){
                if(count[a] > count[h]){
                    int temp = count[a];
                    count[a] = count[h];
                    count[h] = temp;
                    Clinica temporaria = clinicas.get(a);
                    clinicas.set(a, clinicas.get(h));
                    clinicas.set(h, temporaria);
                }
            }
        }

        return clinicas.get(0);
    }

    public int getNumeroClinicas(){
        return clinicas.size();
    }

    public int getNumeroUrgenciasPorClinica(int posicaoClinica, int mes){
        List<Servico> servicos = clinicas.get(posicaoClinica).getServicos();
        int count = 0;
        for(int i = 0; i < servicos.size(); i++){
            if(servicos.get(i).getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("urgência") && servicos.get(i).getDataRealizacao().getMes() == mes)
                count++;
        }
        return count;
    }

    public List<Cliente> getClientesTop(long nif){
        List<Pessoa> p = getClinicaByNif(nif).getPessoas();
        ArrayList<Pessoa> clientesTop = new ArrayList<>();
        ArrayList<Cliente> topCinco = new ArrayList<>();
        for(int i = 0; i < p.size(); i++){
            if(p.get(i) instanceof Cliente)
                clientesTop.add( p.get(i));
        }
        int[] count = new int[clientesTop.size()];
        for(int j = 0; j < count.length; j++){
            count[j] = 0;
            for(int h = 0; h < getClinicaByNif(nif).getServicos().size(); h++){
                if(getClinicaByNif(nif).getServicos().get(h).getIdentificacaoCliente() == clientesTop.get(j).getCartaoCidadao())
                    count[j] += 1;
            }
        }
        List<Pessoa> listaOrganizada = organizarArrays(count, clientesTop);
        for(int n = 0; n < 5; n++){
            topCinco.add((Cliente) listaOrganizada.get(n));
        }
        return topCinco;
    }

    public List<Clinica> organizarClinicas(){
        for(int i = 0; i < clinicas.size() - 1; i++){
            for(int j = i + 1; j < clinicas.size(); j++){
                if(!clinicas.get(j).getDataConstituicao().eAnterior(clinicas.get(i).getDataConstituicao().getDia(), clinicas.get(i).getDataConstituicao().getMes(), clinicas.get(i).getDataConstituicao().getAno())){
                    Clinica temp = clinicas.get(j);
                    clinicas.set(j, clinicas.get(i));
                    clinicas.set(i, temp);
                }
            }
        }
        return clinicas;
    }

    public ArrayList<Dentista> organizarDentistasClinica(long nif){
        Clinica c = getClinicaByNif(nif);
        List<Pessoa> pessoas = c.getPessoas();
        ArrayList<Dentista> dentistas = new ArrayList<>();
        for(int n = 0; n < pessoas.size(); n++){
            if(pessoas.get(n) instanceof Dentista)
                dentistas.add((Dentista) pessoas.get(n));
        }
        for(int i = 0; i < dentistas.size() - 1; i++){
            for(int j = i + 1; j < dentistas.size(); j++){
                if(!dentistas.get(j).getDataNascimento().eAnterior(dentistas.get(i).getDataNascimento().getDia(), dentistas.get(i).getDataNascimento().getMes(), dentistas.get(i).getDataNascimento().getAno())){
                    Dentista temp = dentistas.get(j);
                    dentistas.set(j, dentistas.get(i));
                    dentistas.set(i, temp);
                }
            }
        }
        return dentistas;
    }

    public List<Dentista> getTopDentistas(long nif){
        Clinica c = getClinicaByNif(nif);
        ArrayList<Cliente> clientes = new ArrayList<>();
        for(int i = 0; i < c.getPessoas().size(); i++){
            if(c.getPessoas().get(i) instanceof Cliente)
                clientes.add((Cliente) c.getPessoas().get(i));
        }
        int[] count = new int[organizarDentistasClinica(nif).size()];
        ArrayList<Pessoa> dentistas = new ArrayList<>();
        for(int n = 0; n < c.getPessoas().size(); n++){
            if(c.getPessoas().get(n) instanceof Dentista)
                dentistas.add( c.getPessoas().get(n));
        }
        for(int j = 0; j < count.length; j++){
            for(int n = 0; n < clientes.size(); n++){
                if(clientes.get(n).getIdentificacaoDentista() == dentistas.get(j).getCartaoCidadao()){
                    count[j] += 1;
                }
            }
        }
        List<Pessoa> listaOrdenada = organizarArrays(count, dentistas);
        ArrayList<Dentista> topDentista = new ArrayList<>();
        for(int v = 0; v < 3; v++){
            topDentista.add((Dentista) listaOrdenada.get(v));
        }
        return topDentista;
    }

    public List<Pessoa> organizarArrays(int[] count, List<Pessoa> pessoasAOrganizar){
        for(int a = 0; a < count.length - 1; a++){
            for(int b = a + 1; b < count.length; b++){
                if(count[b] > count[a]){
                    int temp = count[a];
                    count[a] = count[b];
                    count[b] = temp;
                    Pessoa temporaria = pessoasAOrganizar.get(b);
                    pessoasAOrganizar.set(b, pessoasAOrganizar.get(a));
                    pessoasAOrganizar.set(a, temporaria);
                }
            }
        }
        return pessoasAOrganizar;
    }

    public String getTaxa(int posicaoClinica){
        return Double.toString(clinicas.get(posicaoClinica).getTaxa());
    }

    public void adicionarAvaliacao(Avaliacao a, long nif){
        boolean flag = false;
        List<Pessoa> pessoas = clinicas.get(getPosicaoClinicaByNif(nif)).getPessoas();
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i).getCartaoCidadao() == a.getIdentificacaoCliente() && getClinicaByNif(nif).getPessoaByCC(a.getIdentificacaoCliente()) instanceof Cliente)
                flag = true;
        }
        if(!flag)
            throw new CartaoCidadaoNaoEncontradoException("Não existe na clínica nenhum cliente com o número de cartão de cidadão introduzido!");
        else
            clinicas.get(getPosicaoClinicaByNif(nif)).addAvaliacao(a);
    }

    public List<Clinica> getFavoritas(){
        List<Clinica> favoritas = new ArrayList<>();
        for(int i = 0; i < clinicas.size() - 1; i++){
            for(int j = i + 1; j < clinicas.size(); j++){
                if(clinicas.get(j).popularidadeClinica() > clinicas.get(i).popularidadeClinica()){
                    Clinica temp = clinicas.get(j);
                    clinicas.set(j, clinicas.get(i));
                    clinicas.set(i, temp);
                }
            }
        }
        for(int a = 0; a < 3; a++){
            favoritas.add(clinicas.get(a));
        }
        return favoritas;
    }

}
