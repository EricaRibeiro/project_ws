package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.ArrayList;


@JacksonXmlRootElement(localName = "servicos")
public class ListaServicoParcialDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "servico")

    private ArrayList<ServicoParcialDTO> servicos;

    public ListaServicoParcialDTO() {
        this.servicos = new ArrayList<>();
    }

    public ArrayList<ServicoParcialDTO> getServicos() {
        return servicos;
    }
    public void setServicos(ArrayList<ServicoParcialDTO> servicos) {
        this.servicos = servicos;
    }
}
