package pt.isep.clinic.exception;

public class NumeroCedulaDuplicadoException extends RuntimeException{
    public NumeroCedulaDuplicadoException(String s) {
        super(s);
    }
}
