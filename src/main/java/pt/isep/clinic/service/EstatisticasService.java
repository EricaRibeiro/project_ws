package pt.isep.clinic.service;
import pt.isep.clinic.dao.Dados;
import pt.isep.clinic.dto.*;
import pt.isep.clinic.exception.DataInvalidaException;
import pt.isep.clinic.exception.NifNaoEncontradoException;
import pt.isep.clinic.exception.NumeroInvalidoException;
import pt.isep.clinic.model.*;
import java.util.ArrayList;
import java.util.List;

public class EstatisticasService {

    public static ListaClinicaParcialDTO getClinicaMaisUrgencias(){
        Gestao g = Dados.carregarDados();
        ArrayList<Clinica> clinicas = new ArrayList<>();
        Clinica clinica = g.buscarClinicaMaisUrgencias();
        clinicas.add(clinica);
        ListaClinicaParcialDTO c = Converter.listaClinica2ListaClinicaParcialDTO(clinicas);
        return c;
    }

    public static ListaUrgenciasMesDTO getNumeroUrgencias(int mes){
        if(mes < 1 || mes > 12)
            throw new DataInvalidaException("O mês introduzido não é válido!");
        else{
            Gestao g = Dados.carregarDados();
            ArrayList<UrgenciasMes> numeroUrgencias = new ArrayList<>();
            for(int i = 0; i < g.getNumeroClinicas(); i++){
                UrgenciasMes u = new UrgenciasMes(g.getClinicas().get(i).getNome(), g.getNumeroUrgenciasPorClinica(i, mes));
                numeroUrgencias.add(u);
            }
            ListaUrgenciasMesDTO lista = Converter.listaUrgenciasMes2ListaUrgenciasMesDTO(numeroUrgencias);
            return lista;
        }
    }

    public static ListaClienteParcialDTO getTopCincoClientes(long nif){
        Gestao g = Dados.carregarDados();
        if(g.getPosicaoClinicaByNif(nif) != -1){
            List<Cliente> clientesTop = g.getClientesTop(nif);
            ListaClienteParcialDTO clientes = Converter.listaCliente2ListaClienteParcialDTO((ArrayList<Cliente>) clientesTop);
            return clientes;
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaClinicaParcialDTO getClinicasOrganizadas(){
        Gestao g = Dados.carregarDados();
        List<Clinica> clinicas = g.organizarClinicas();
        ListaClinicaParcialDTO listaOrganizada = Converter.listaClinica2ListaClinicaParcialDTO((ArrayList<Clinica>) clinicas);
        return listaOrganizada;
    }

    public static ListaDentistaParcialDTO getDentistasOrganizados(long nif){
        Gestao g = Dados.carregarDados();
        if(g.getPosicaoClinicaByNif(nif) != - 1){
            ArrayList<Dentista> dentistas = g.organizarDentistasClinica(nif);
            ListaDentistaParcialDTO listaOrganizada = Converter.listaDentista2ListaDentistaParcialDTO(dentistas);
            return listaOrganizada;
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaDentistaParcialDTO getTop3DentistasMaisClientes(long nif){
        Gestao g = Dados.carregarDados();
        if(g.getPosicaoClinicaByNif(nif) != -1){
            List<Dentista> top = g.getTopDentistas(nif);
            ListaDentistaParcialDTO listaDoTop = Converter.listaDentista2ListaDentistaParcialDTO((ArrayList<Dentista>) top);
            return listaDoTop;
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaTaxaUrgenciaDTO listaTaxaUrgencia(){
        Gestao g = Dados.carregarDados();
        ArrayList<TaxaUrgencia> taxaUrgencia = new ArrayList<>();
        for(int i = 0; i < g.getNumeroClinicas(); i++){
            TaxaUrgencia t = new TaxaUrgencia(g.getClinicas().get(i).getNome(), g.getTaxa(i));
            taxaUrgencia.add(t);
        }

        ListaTaxaUrgenciaDTO taxaUrgenciaDTO = Converter.listaTaxaUrgencia2ListaTaxaUrgenciaDTO(taxaUrgencia);

        return taxaUrgenciaDTO;
    }

    public static void novaAvaliacao(AvaliacaoDTO a, long nif){
        Gestao g = Dados.carregarDados();
        if(g.getPosicaoClinicaByNif(nif) != -1){
            if(a.getAvaliacao() < 1 || a.getAvaliacao() > 5)
                throw new NumeroInvalidoException("A avaliação efetuada pelo cliente deve ser um valor compreendido entre 1 e 5, sendo 1 a pior classificação e 5 a melhor classificação!");
            Avaliacao avaliacao = Converter.avaliacaoDTO2Avaliacao(a);
            g.adicionarAvaliacao(avaliacao, nif);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaClinicaParcialDTO getClinicasFavoritas(){
        Gestao g = Dados.carregarDados();
        List<Clinica> lista = g.getFavoritas();
        ListaClinicaParcialDTO listaFavortias = Converter.listaClinica2ListaClinicaParcialDTO((ArrayList<Clinica>) lista);

        return listaFavortias;
    }
}
