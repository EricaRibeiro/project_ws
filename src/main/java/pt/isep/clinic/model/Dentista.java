package pt.isep.clinic.model;
import java.io.Serializable;

public class Dentista extends Pessoa implements Serializable {
    private long numeroCedula;

    public Dentista(String nome, long cartaoCidadao, Data dataNascimento, long numeroCedula){
        super(nome, cartaoCidadao, dataNascimento);
        this.numeroCedula = numeroCedula;
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
