package pt.isep.clinic.service;
import pt.isep.clinic.dao.Dados;
import pt.isep.clinic.dto.ClinicaDTO;
import pt.isep.clinic.dto.Converter;
import pt.isep.clinic.dto.ListaClinicaParcialDTO;
import pt.isep.clinic.model.*;

import java.util.ArrayList;
import java.util.List;

public class ClinicasService {

    public static void adicionarClinica(ClinicaDTO clinica){
        Gestao g = Dados.carregarDados();
        Clinica c = Converter.clinicaDTO2Clinica(clinica);
        g.adicionarClinica(c);
        Dados.guardarDados(g);
    }

    public static ListaClinicaParcialDTO getClinicas(){
        Gestao g = Dados.carregarDados();
        List<Clinica> clinicas = g.getClinicas();
        ListaClinicaParcialDTO listaClinicaParcialDTO = Converter.listaClinica2ListaClinicaParcialDTO((ArrayList<Clinica>) clinicas);
        return listaClinicaParcialDTO;
    }

    public static void eliminarClinica(long nif){
        Gestao g = Dados.carregarDados();
        g.eliminarClinica(nif);
        Dados.guardarDados(g);
    }

    public static void alterarClinica(long nif, ClinicaDTO c){
        Gestao g = Dados.carregarDados();
        Clinica clinicaRegistada = g.getClinicaByNif(nif);
        Clinica clinica = Converter.clinicaDTO2Clinica(c);
        clinicaRegistada.setNif(clinica.getNif());
        clinicaRegistada.setNome(clinica.getNome());
        clinicaRegistada.setDataConstituicao(clinica.getDataConstituicao());
        clinicaRegistada.setServicos(clinicaRegistada.getServicos());
        clinicaRegistada.setAvaliacoes(clinicaRegistada.getAvaliacoes());
        clinicaRegistada.setPessoas(clinicaRegistada.getPessoas());
        Dados.guardarDados(g);
    }

    public static ClinicaDTO getClinica(long nif){
        Gestao g = Dados.carregarDados();
        for(int i = 0; i < g.getNumeroClinicas(); i++){
            if(g.getClinicas().get(i).getNif() == nif){
                Clinica c = g.getClinicaByNif(nif);
                ClinicaDTO clinica = Converter.clinica2ClinicaDTO(c);
                return clinica;
            }
        }
        return null;
    }
}
