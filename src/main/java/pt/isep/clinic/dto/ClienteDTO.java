package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"identificacaoDentista"})
@JacksonXmlRootElement(localName = "cliente")
public class ClienteDTO extends PessoaDTO {

    @JacksonXmlProperty(localName = "identificacaoDentista")
    private long identificacaoDentista;

    public ClienteDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long identificacaoDentista){
        super(nome, cartaoCidadao, dataNascimento);
        this.identificacaoDentista = identificacaoDentista;
    }

    public ClienteDTO(){
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
