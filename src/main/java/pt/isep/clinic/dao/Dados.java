package pt.isep.clinic.dao;
import pt.isep.clinic.model.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.file.StandardOpenOption.CREATE;

public class Dados {
    static final String GESTAO_FILE ="gestao_dados.dat";

    public static Gestao carregarDados(){
        Gestao gestao = new Gestao();
        Path file = Paths.get(GESTAO_FILE);
        try {
            ObjectInputStream o = new ObjectInputStream(new FileInputStream(file.toString()));
            gestao = (Gestao)o.readObject();
            o.close();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return gestao;
    }

    public static void guardarDados(Gestao gestao) {
        Path file = Paths.get(GESTAO_FILE);
        try{
            ObjectOutputStream o = new ObjectOutputStream(Files.newOutputStream(file, CREATE));
            o.writeObject(gestao);
            o.close();
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
