package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"nome", "taxa"})
@JacksonXmlRootElement(localName = "taxaUrgencia")
public class TaxaUrgenciaDTO {

    @JacksonXmlProperty(localName = "nome")
    private String nomeClinica;
    @JacksonXmlProperty(localName = "taxa")
    private String taxaUrgencia;

    public TaxaUrgenciaDTO(String nomeClinica, String taxaUrgencia){
        this.taxaUrgencia = taxaUrgencia;
        this.nomeClinica = nomeClinica;
    }

    public TaxaUrgenciaDTO(){}

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public String getTaxaUrgencia() {
        return taxaUrgencia;
    }

    public void setTaxaUrgencia(String taxaUrgencia) {
        this.taxaUrgencia = taxaUrgencia;
    }


}
