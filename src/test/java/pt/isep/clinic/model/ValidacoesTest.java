package pt.isep.clinic.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidacoesTest {

    @Test
    void validarDiaDaSemana() {
        Data d = new Data(16, 5, 2021);
        boolean resultado = Validacoes.validarDiaDaSemana(d);

        assertEquals(true, resultado);

    }
}