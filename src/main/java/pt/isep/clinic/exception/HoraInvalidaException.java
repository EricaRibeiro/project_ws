package pt.isep.clinic.exception;

public class HoraInvalidaException extends RuntimeException{
    public HoraInvalidaException(String s) {
        super(s);
    }
}
