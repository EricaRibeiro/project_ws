package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"codigoConsulta", "tipoServico", "data", "hora", "identificacaoCliente"})
@JacksonXmlRootElement(localName = "servico")
public class ServicoParcialDTO {

    @JacksonXmlProperty(localName = "tipoServico")
    private String tipoServico;
    @JacksonXmlProperty(localName = "data")
    private DataDTO data;
    @JacksonXmlProperty(localName = "hora")
    private HoraDTO hora;
    @JacksonXmlProperty(localName = "identificacaoCliente")
    private long identificacaoCLiente;
    @JacksonXmlProperty(localName = "codigoConsulta")
    private int codigoConsulta;

    public ServicoParcialDTO(){

    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    public HoraDTO getHora() {
        return hora;
    }

    public void setHora(HoraDTO hora) {
        this.hora = hora;
    }

    public long getIdentificacaoCLiente() {
        return identificacaoCLiente;
    }

    public void setIdentificacaoCLiente(long identificacaoCLiente) {
        this.identificacaoCLiente = identificacaoCLiente;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

}
