package pt.isep.clinic.exception;

public class CodigoConsultaDuplicadoException extends RuntimeException{
    public CodigoConsultaDuplicadoException(String s) {
        super(s);
    }
}
