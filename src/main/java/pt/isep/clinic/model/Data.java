package pt.isep.clinic.model;
import pt.isep.clinic.exception.DataInvalidaException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Data implements Serializable {
    private int dia;
    private int mes;
    private int ano;

    public Data(int dia, int mes, int ano){
        verificarData(dia, mes, ano);
    }

    public Data(Data data){
        verificarData(data.dia, data.mes, data.ano);
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setData(int dia, int mes, int ano){
        verificarData(dia, mes, ano);
    }

    private void verificarData(int dia, int mes, int ano) throws DataInvalidaException {
        if(validarValoresData(dia, mes, ano))
        {
            this.dia = dia;
            this.mes = mes;
            this.ano = ano;
        }
        else
            throw new DataInvalidaException("Atenção: " + dia + "-" + mes + "-" + ano + " = data inválida!");
    }

    private boolean validarValoresData(int dia, int mes, int ano){
        if(ano < 1000 || mes < 0 || mes > 11)
            return false;
        else{
            if((mes == 0 || mes == 2 || mes == 4 || mes == 6 || mes == 7 || mes == 9 || mes == 11) && dia < 1 || dia > 31)
                return false;
            else{
                if((mes == 3 || mes == 5 || mes == 8 || mes == 10) && (dia < 1 || dia > 30))
                    return false;
                else{
                    if(mes == 1 && (((ano % 4 == 0) && (ano % 100 != 0)) || (ano % 400 == 0)) && (dia < 1 || dia > 29))
                        return false;
                    else {
                        if(mes == 1 && (dia < 1 || dia > 28) && ano % 4 != 0){
                            return false;

                        }
                    }
                }
            }
        }
        return true;
    }

    public boolean eAnterior(int dia, int mes, int ano){
        if(this.ano < ano)
            return false;
        else{
            if(this.mes < mes)
                return false;
            else{
                if(this.dia < dia)
                    return false;
            }
        }
        return true;
    }

    public static Data obterDataAtual(){
        return new Data(Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.MONTH) + 1, Calendar.getInstance().get(Calendar.YEAR));
    }

    public static boolean validarDataTipoServicoConsulta(Data dataRealizacao, Data testeJaFeito) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        long diferenciaEmDias;
        Date dateJaFeito = sdf.parse("" + testeJaFeito.getDia() + "-" + testeJaFeito.getMes() + "-" + testeJaFeito.getAno());
        Date dateRealizadoAgora = sdf.parse("" + dataRealizacao.getDia() + "-" + dataRealizacao.getMes() + "-" + dataRealizacao.getAno());
        long differenceMilliSeconds = dateRealizadoAgora.getTime() - dateJaFeito.getTime();
        diferenciaEmDias = differenceMilliSeconds/1000/60/60/24;
        if(diferenciaEmDias < 30)
            return false;

        return true;
    }
}
