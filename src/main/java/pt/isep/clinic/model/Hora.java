package pt.isep.clinic.model;
import pt.isep.clinic.exception.HoraInvalidaException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Hora implements Serializable {

    private int hora;
    private int minutos;

    public Hora(int hora, int minutos){
        this.hora = hora;
        this.minutos = minutos;
    }

    public static boolean validarHora(int hora, Servico consulta){
        String a = consulta.getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "");
        if(a.equals("urgência")){
            if(hora < 8 || hora > 22)
                throw new HoraInvalidaException("Impossível proceder ao agendamento da consulta! A hora inserida não é válida!");
            else
                return true;
        }
        else {
            if((hora > 8 && hora < 12 || hora > 13 && hora < 20) && !Validacoes.validarDiaDaSemana(consulta.getDataRealizacao()))
                return true;
            else
                throw new HoraInvalidaException("Impossível proceder ao agendamento da consulta! A hora inserida não é válida/o tipo de serviço introduzido não pode ser realizado nesse horário!");
        }
    }

    public static boolean validarMinutos(int minutos){
        if(minutos % 30 != 0 ){
            throw new HoraInvalidaException("Impossível proceder ao agendamento da consulta! Os minutos devem ser múltiplos de 30 (só são marcadas consultas de meia em meia hora)!");
        }
        return true;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public boolean eAnterior(int hora, int minuto, Data d){
        if(d.getAno() == Data.obterDataAtual().getAno() && d.getDia() == Data.obterDataAtual().getDia() && d.getMes() == Data.obterDataAtual().getMes()){
            if(this.hora < hora)
                return false;
            else{
                if(this.hora == hora && this.minutos < minuto)
                    return false;
            }
        }
        return true;
    }

    public static Hora obterHoraAtual(){
        LocalDateTime agora = LocalDateTime.now();
        DateTimeFormatter formatarHora = DateTimeFormatter.ofPattern("HH:mm");
        String[] separado = formatarHora.format(agora).split(":");
        return new Hora(Integer.parseInt(separado[0]), Integer.parseInt(separado[1]));
    }
}
