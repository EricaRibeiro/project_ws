package pt.isep.clinic.model;
import java.io.Serializable;

public class TaxaUrgencia implements Serializable {

    private String nomeClinica;
    private String taxaUrgencia;

    public TaxaUrgencia(String nomeClinica, String taxaUrgencia){
        this.taxaUrgencia = taxaUrgencia;
        this.nomeClinica = nomeClinica;
    }

    public TaxaUrgencia(){}

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public String getTaxaUrgencia() {
        return taxaUrgencia;
    }

    public void setTaxaUrgencia(String taxaUrgencia) {
        this.taxaUrgencia = taxaUrgencia;
    }

}
