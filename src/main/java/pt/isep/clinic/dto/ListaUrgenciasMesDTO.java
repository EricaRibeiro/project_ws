package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.ArrayList;

@JacksonXmlRootElement(localName = "urgenciasMes")
public class ListaUrgenciasMesDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "urgenciaMes")

    private ArrayList<UrgenciasMesDTO> urgencias;

    public ListaUrgenciasMesDTO() {
        this.urgencias = new ArrayList<>();
    }

    public ArrayList<UrgenciasMesDTO> getUrgencias() {
        return urgencias;
    }
    public void setUrgencias(ArrayList<UrgenciasMesDTO> urgencias) {
        this.urgencias = urgencias;
    }

}
