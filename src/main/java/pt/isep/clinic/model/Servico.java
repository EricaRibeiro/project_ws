package pt.isep.clinic.model;

import java.io.Serializable;
import java.util.Objects;

public class Servico implements Serializable {

    private String tipoServico;
    private int preco;
    private Data dataRealizacao;
    private Hora horaRealizacao;
    private long identificacaoCliente;
    private int codigoConsulta;

    public Servico(String tipoServico, int preco, Data dataRealizacao, Hora horaRealizacao, long identificacaoCliente, int codigoConsulta){
        this.tipoServico = tipoServico;
        this.preco = preco;
        this.dataRealizacao = dataRealizacao;
        this.horaRealizacao = horaRealizacao;
        this.identificacaoCliente = identificacaoCliente;
        this.codigoConsulta = codigoConsulta;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public int getPreco() {
        return preco;
    }

    public void setPreco(int preco) {
        this.preco = preco;
    }

    public Data getDataRealizacao() {
        return dataRealizacao;
    }

    public void setDataRealizacao(Data dataRealizacao) {
        this.dataRealizacao = dataRealizacao;
    }

    public Hora getHoraRealizacao() {
        return horaRealizacao;
    }

    public void setHoraRealizacao(Hora horaRealizacao) {
        this.horaRealizacao = horaRealizacao;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

    public int getCodigoConsulta() {
        return codigoConsulta;
    }

    public void setCodigoConsulta(int codigoConsulta) {
        this.codigoConsulta = codigoConsulta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servico servico = (Servico) o;
        return Objects.equals(horaRealizacao, servico.horaRealizacao);
    }
}
