package pt.isep.clinic.model;
import java.io.Serializable;

public class Cliente extends Pessoa implements Serializable {

    private long identificacaoDentista;

    public Cliente(String nome, long cartaoCidadao, Data dataNascimento, long identificacaoDentista) {
        super(nome, cartaoCidadao, dataNascimento);
        this.identificacaoDentista = identificacaoDentista;
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
