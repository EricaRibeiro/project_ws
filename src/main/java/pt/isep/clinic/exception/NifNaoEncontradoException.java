package pt.isep.clinic.exception;

public class NifNaoEncontradoException extends RuntimeException{
    public NifNaoEncontradoException(String s) {
        super(s);
    }
}
