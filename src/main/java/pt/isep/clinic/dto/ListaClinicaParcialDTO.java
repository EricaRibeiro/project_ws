package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.ArrayList;

@JacksonXmlRootElement(localName = "clinicas")
public class ListaClinicaParcialDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "clinica")

    private ArrayList<ClinicaParcialDTO> clinicas;

    public ListaClinicaParcialDTO() {
        this.clinicas = new ArrayList<>();
    }

    public ArrayList<ClinicaParcialDTO> getClinicas() {
        return clinicas;
    }
    public void setClinicas(ArrayList<ClinicaParcialDTO> clinicas) {
        this.clinicas = clinicas;
    }
}
