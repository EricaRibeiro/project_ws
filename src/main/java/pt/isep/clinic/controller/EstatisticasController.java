package pt.isep.clinic.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isep.clinic.dto.*;
import pt.isep.clinic.service.EstatisticasService;

@RestController
@RequestMapping("/api")
public class EstatisticasController {

    @RequestMapping(value = "/maisUrgencias",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getClinicaMaisUrgencias(){
        try{
            ListaClinicaParcialDTO c = EstatisticasService.getClinicaMaisUrgencias();
            return new ResponseEntity<>(c, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/numeroUrgenciasNumMes/{mes}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getNumeroUrgencias(@PathVariable("mes") int mes) {
        try{
            ListaUrgenciasMesDTO numeroUrgencias = EstatisticasService.getNumeroUrgencias(mes);
            return new ResponseEntity<>(numeroUrgencias, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/clientesAssiduos",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getTopCincoClientes(@PathVariable("nif") long nif) {
        try{
            ListaClienteParcialDTO clientes = EstatisticasService.getTopCincoClientes(nif);
            return new ResponseEntity<>(clientes, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/ordemDasClinicas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getListaClinicasOrganizada(){
        try{
            ListaClinicaParcialDTO c = EstatisticasService.getClinicasOrganizadas();
            return new ResponseEntity<>(c, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/ordemDosDentistas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getListaDentistasOrganizada(@PathVariable("nif") long nif) {
        try{
            ListaDentistaParcialDTO lista = EstatisticasService.getDentistasOrganizados(nif);
            return new ResponseEntity<>(lista, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/topDosDentistas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getTopDentistas(@PathVariable("nif") long nif){
        try{
            ListaDentistaParcialDTO l = EstatisticasService.getTop3DentistasMaisClientes(nif);
            return new ResponseEntity<>(l, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/taxaUrgencia",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getTaxaUrgenciaCada(){
        try{
            ListaTaxaUrgenciaDTO lista = EstatisticasService.listaTaxaUrgencia();
            return new ResponseEntity<>(lista, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/avaliarClinica/{nif}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_XML_VALUE,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> avaliarClinica(@PathVariable("nif") long nif, @RequestBody AvaliacaoDTO avaliacao) {
        try{
            EstatisticasService.novaAvaliacao(avaliacao, nif);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/topClinicasFavoritas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getClinicasFavoritas(){
        try{
            ListaClinicaParcialDTO lista = EstatisticasService.getClinicasFavoritas();
            return new ResponseEntity<>(lista, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

}
