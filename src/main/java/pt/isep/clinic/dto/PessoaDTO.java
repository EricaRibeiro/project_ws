package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"nome", "cartaoCidadao", "data"})
@JacksonXmlRootElement(localName = "pessoa")
public class PessoaDTO {
    @JacksonXmlProperty(localName = "nome")
    private String nome;
    @JacksonXmlProperty(localName = "cartaoCidadao")
    private long cartaoCidadao;
    @JacksonXmlProperty(localName = "data")
    private DataDTO dataNascimento;

    public PessoaDTO(String nome, long cartaoCidadao, DataDTO dataNascimento){
        this.nome = nome;
        this.cartaoCidadao = cartaoCidadao;
        this.dataNascimento = dataNascimento;
    }

    public PessoaDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        this.cartaoCidadao = cartaoCidadao;
    }

    public DataDTO getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(DataDTO dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
