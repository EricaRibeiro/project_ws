package pt.isep.clinic.exception;

public class ImpossivelAdicionarConsultaException extends RuntimeException{
    public ImpossivelAdicionarConsultaException(String s){ super(s);}
}
