package pt.isep.clinic.exception;

public class NumeroInvalidoException extends RuntimeException{
    public NumeroInvalidoException(String s){ super(s);}
}
