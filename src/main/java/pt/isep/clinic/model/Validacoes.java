package pt.isep.clinic.model;
import java.util.Calendar;
import java.util.Locale;

public class Validacoes {

    public static boolean validarTamanhoNumeros(long n){
        String a = Long.toString(n);
        if(a.length() != 9){
            System.out.println("O número inserido deve ter 9 dígitos!");
            return false;
        }
        return true;
    }

    public static boolean validarTipoServico(String servico){
        String novaString = servico.toLowerCase(Locale.ROOT).trim().replace(" ", "");
        if(!novaString.equals("limpeza") && !novaString.equals("consultaderotina") && !novaString.equals("colocaçãodeaparelho") && !novaString.equals("manutençãodeaparelho") && !novaString.equals("desvitalização") && !novaString.equals("urgência") && !novaString.equals("removerdente"))
           return false;
        return true;
    }

    public static boolean validarCodigoConsulta(int codigo){
        String b = Integer.toString(codigo);
        if(b.length() != 4){
            return false;
        }
        return true;
    }

    public static boolean validarDiaDaSemana(Data d){
        Calendar c = Calendar.getInstance();
        c.set(d.getAno(), d.getMes() -1, d.getDia());
        return c.get(Calendar.DAY_OF_WEEK) == 1;
    }
}
