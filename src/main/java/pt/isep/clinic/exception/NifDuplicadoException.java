package pt.isep.clinic.exception;

public class NifDuplicadoException extends RuntimeException{
    public NifDuplicadoException(String s) {
        super(s);
    }
}
