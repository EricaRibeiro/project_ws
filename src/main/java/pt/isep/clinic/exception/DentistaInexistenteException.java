package pt.isep.clinic.exception;

public class DentistaInexistenteException extends RuntimeException{
    public DentistaInexistenteException(String s) {
        super(s);
    }
}
