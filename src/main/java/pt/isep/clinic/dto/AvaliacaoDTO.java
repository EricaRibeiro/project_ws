package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"identificacaoCliente", "avaliacao"})
@JacksonXmlRootElement(localName = "avaliacaoClinica")
public class AvaliacaoDTO {

    @JacksonXmlProperty(localName = "identificacaoCliente")
    private long identificacaoCliente;
    @JacksonXmlProperty(localName = "avaliacao")
    private int avaliacao;

    public AvaliacaoDTO(){

    }

    public AvaliacaoDTO(int avaliacao, long identificacaoCliente){
        this.avaliacao = avaliacao;
        this.identificacaoCliente = identificacaoCliente;
    }

    public long getIdentificacaoCliente() {
        return identificacaoCliente;
    }

    public void setIdentificacaoCliente(long identificacaoCliente) {
        this.identificacaoCliente = identificacaoCliente;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }
}
