package pt.isep.clinic.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isep.clinic.dto.*;
import pt.isep.clinic.service.PessoasService;

@RestController
@RequestMapping("/api")
public class PessoasController {

    @RequestMapping(value = "/clinica/{nif}/dentista",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> addPessoa(@PathVariable("nif") long nif, @RequestBody DentistaDTO dentistaDTO){
        try{
            PessoasService.adicionarDentista(dentistaDTO, nif);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/cliente",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> addPessoa(@PathVariable("nif") long nif, @RequestBody ClienteDTO clienteDTO) {
        try{
            PessoasService.adicionarCliente(clienteDTO, nif);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/pessoa/{cartaoCidadao}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> deletePessoa(@PathVariable("nif") long nif, @PathVariable("cartaoCidadao") long cartaoCidadao){
        try{
            PessoasService.eliminarPessoa(nif, cartaoCidadao);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinicas/{nif}/dentistas/{cartaoCidadao}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> updateDentista(@PathVariable("nif") long nif, @PathVariable("cartaoCidadao") long cartaoCidadao, @RequestBody DentistaDTO dentistaDTO){
        try{
            PessoasService.alterarDentista(nif, cartaoCidadao, dentistaDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinicas/{nif}/clientes/{cartaoCidadao}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> updateCliente(@PathVariable("nif") long nif, @PathVariable("cartaoCidadao") long cartaoCidadao, @RequestBody ClienteDTO clienteDTO){
        try{
            PessoasService.alterarCliente(nif, cartaoCidadao, clienteDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/dentistas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> listDentistas(@PathVariable("nif") long nif) {
        try{
            ListaDentistaParcialDTO listaDentistaParcialDTO = PessoasService.getSoDentistas(nif);
            return new ResponseEntity<>(listaDentistaParcialDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/dentista/{cartaoCidadao}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getDentista(@PathVariable("nif") long nif, @PathVariable("cartaoCidadao") long cartaoCidadao) {
        try{
            DentistaDTO dentistaDTO = PessoasService.getDentista(nif, cartaoCidadao);
            return new ResponseEntity<>(dentistaDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/clientes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> listClientes(@PathVariable("nif") long nif) {
        try{
            ListaClienteParcialDTO listaClienteParcialDTO = PessoasService.getSoClientes(nif);
            return new ResponseEntity<>(listaClienteParcialDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/pessoaCliente/{cartaoCidadao}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getCliente(@PathVariable("nif") long nif, @PathVariable("cartaoCidadao") long cartaoCidadao) {
        try{
            ClienteDTO clienteDTO = PessoasService.getCliente(nif, cartaoCidadao);
            return new ResponseEntity<>(clienteDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

}
