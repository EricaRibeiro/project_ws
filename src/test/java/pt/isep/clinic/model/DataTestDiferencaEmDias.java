package pt.isep.clinic.model;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class DataTestDiferencaEmDias {

    @Test
    void validarDataTipoServicoConsulta() throws ParseException {

        Data d = new Data(12, 3, 2020);
        Data e = new Data(20, 3, 2020);

        boolean resultado = Data.validarDataTipoServicoConsulta(d, e);

        assertEquals(false, resultado);
    }
}