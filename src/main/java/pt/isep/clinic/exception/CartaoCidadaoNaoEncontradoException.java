package pt.isep.clinic.exception;

public class CartaoCidadaoNaoEncontradoException extends RuntimeException{
    public CartaoCidadaoNaoEncontradoException(String s) {
        super(s);
    }
}
