package pt.isep.clinic.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"hora", "minutos"})
@JacksonXmlRootElement(localName = "horario")
public class HoraDTO {

    @JacksonXmlProperty(localName = "hora")
    private int hora;
    @JacksonXmlProperty(localName = "minutos")
    private int minutos;

    public HoraDTO(){

    }

    public HoraDTO(int hora, int minutos){
        this.hora = hora;
        this.minutos = minutos;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }
}
