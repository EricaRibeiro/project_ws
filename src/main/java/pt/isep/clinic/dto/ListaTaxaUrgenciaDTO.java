package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.ArrayList;

@JacksonXmlRootElement(localName = "taxasUrgencias")
public class ListaTaxaUrgenciaDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "taxaUrgencia")

    private ArrayList<TaxaUrgenciaDTO> taxas;

    public ListaTaxaUrgenciaDTO() {
        this.taxas = new ArrayList<>();
    }

    public ArrayList<TaxaUrgenciaDTO> getTaxas() {
        return taxas;
    }
    public void setTaxas(ArrayList<TaxaUrgenciaDTO> taxas) {
        this.taxas = taxas;
    }

}
