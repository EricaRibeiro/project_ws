package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"numeroCedula"})
@JacksonXmlRootElement(localName = "dentista")
public class DentistaDTO extends PessoaDTO {
    @JacksonXmlProperty(localName = "numeroCedula")
    private long numeroCedula;

    public DentistaDTO(String nome, long cartaoCidadao, DataDTO dataNascimento, long numeroCedula){
        super(nome, cartaoCidadao, dataNascimento);
        this.numeroCedula = numeroCedula;
    }

    public DentistaDTO(){
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
