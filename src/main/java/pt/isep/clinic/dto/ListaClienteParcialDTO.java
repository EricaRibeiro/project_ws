package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;

@JacksonXmlRootElement(localName = "clientes")
public class ListaClienteParcialDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "cliente")

    private ArrayList<ClienteParcialDTO> clientes;

    public ListaClienteParcialDTO() {
        this.clientes = new ArrayList<>();
    }

    public ArrayList<ClienteParcialDTO> getClientes() {
        return clientes;
    }
    public void setClientes(ArrayList<ClienteParcialDTO> clientes) {
        this.clientes = clientes;
    }
}
