package pt.isep.clinic.exception;

public class CodigoConsultaInvalidoException extends RuntimeException{
    public CodigoConsultaInvalidoException(String s) {
        super(s);
    }
}
