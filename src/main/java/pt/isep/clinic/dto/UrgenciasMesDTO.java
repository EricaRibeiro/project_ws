package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"nomeClinica", "numeroUrgencias"})
@JacksonXmlRootElement(localName = "urgenciaMes")
public class UrgenciasMesDTO {

    @JacksonXmlProperty(localName = "nomeClinica")
    private String nomeClinica;
    @JacksonXmlProperty(localName = "numeroUrgencias")
    private int numeroUrgencias;

    public UrgenciasMesDTO(){}

    public UrgenciasMesDTO(String nomeClinica, int numeroUrgencias){
        this.nomeClinica = nomeClinica;
        this.numeroUrgencias = numeroUrgencias;
    }

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public int getNumeroUrgencias() {
        return numeroUrgencias;
    }

    public void setNumeroUrgencias(int numeroUrgencias) {
        this.numeroUrgencias = numeroUrgencias;
    }
}
