package pt.isep.clinic.model;
import java.io.Serializable;

public class UrgenciasMes implements Serializable {

    private String nomeClinica;
    private int numeroUrgencias;

    public UrgenciasMes(){}

    public UrgenciasMes(String nomeClinica, int numeroUrgencias){
        this.nomeClinica = nomeClinica;
        this.numeroUrgencias = numeroUrgencias;
    }

    public String getNomeClinica() {
        return nomeClinica;
    }

    public void setNomeClinica(String nomeClinica) {
        this.nomeClinica = nomeClinica;
    }

    public int getNumeroUrgencias() {
        return numeroUrgencias;
    }

    public void setNumeroUrgencias(int numeroUrgencias) {
        this.numeroUrgencias = numeroUrgencias;
    }

}
