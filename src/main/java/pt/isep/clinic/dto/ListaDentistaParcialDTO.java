package pt.isep.clinic.dto;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import java.util.ArrayList;

@JacksonXmlRootElement(localName = "dentistas")
public class ListaDentistaParcialDTO {

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "dentista")
    private ArrayList<DentistaParcialDTO> dentistas;

    public ListaDentistaParcialDTO() {
    }

    public ArrayList<DentistaParcialDTO> getDentistas() {
        return dentistas;
    }

    public void setDentistas(ArrayList<DentistaParcialDTO> dentistas) {
        this.dentistas = dentistas;
    }

}
