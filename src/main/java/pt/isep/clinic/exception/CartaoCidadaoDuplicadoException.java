package pt.isep.clinic.exception;

public class CartaoCidadaoDuplicadoException extends RuntimeException{
    public CartaoCidadaoDuplicadoException(String s) {
        super(s);
    }
}
