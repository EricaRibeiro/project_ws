package pt.isep.clinic.exception;

public class PessoaJaExistenteNaClinicaException extends RuntimeException{
    public PessoaJaExistenteNaClinicaException(String s) {
        super(s);
    }
}
