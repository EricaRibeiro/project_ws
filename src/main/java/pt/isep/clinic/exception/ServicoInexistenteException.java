package pt.isep.clinic.exception;

public class ServicoInexistenteException extends RuntimeException{
    public ServicoInexistenteException(String s) {
        super(s);
    }
}
