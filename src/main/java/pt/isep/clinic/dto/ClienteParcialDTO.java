package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"identificacaoDentista"})
@JacksonXmlRootElement(localName = "cliente")
public class ClienteParcialDTO extends PessoaParcialDTO{

    @JacksonXmlProperty(localName = "identificacaoDentista")
    private long identificacaoDentista;

    public ClienteParcialDTO(){
    }

    public long getIdentificacaoDentista() {
        return identificacaoDentista;
    }

    public void setIdentificacaoDentista(long identificacaoDentista) {
        this.identificacaoDentista = identificacaoDentista;
    }
}
