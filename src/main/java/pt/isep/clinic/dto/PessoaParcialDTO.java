package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"nome", "cartaoCidadao"})
@JacksonXmlRootElement(localName = "pessoa")
public class PessoaParcialDTO {

    @JacksonXmlProperty(localName = "nome")
    private String nome;
    @JacksonXmlProperty(localName = "cartaoCidadao")
    private long cartaoCidadao;

    public PessoaParcialDTO(){
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        this.cartaoCidadao = cartaoCidadao;
    }
}
