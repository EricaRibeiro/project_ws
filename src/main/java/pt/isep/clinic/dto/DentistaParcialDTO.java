package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"numeroCedula"})
@JacksonXmlRootElement(localName = "dentista")
public class DentistaParcialDTO extends PessoaParcialDTO{

    @JacksonXmlProperty(localName = "numeroCedula")
    private long numeroCedula;

    public DentistaParcialDTO(){
    }

    public long getNumeroCedula() {
        return numeroCedula;
    }

    public void setNumeroCedula(long numeroCedula) {
        this.numeroCedula = numeroCedula;
    }
}
