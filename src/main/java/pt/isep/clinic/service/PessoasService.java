package pt.isep.clinic.service;
import pt.isep.clinic.dao.Dados;
import pt.isep.clinic.dto.*;
import pt.isep.clinic.exception.CartaoCidadaoNaoEncontradoException;
import pt.isep.clinic.exception.NifNaoEncontradoException;
import pt.isep.clinic.exception.TipoPessoaInvalidoException;
import pt.isep.clinic.model.*;
import java.util.ArrayList;
import java.util.List;

public class PessoasService {
    public static void adicionarDentista(DentistaDTO dentistaDTO, long nif) throws NifNaoEncontradoException {
        Gestao g = Dados.carregarDados();
        int posicaoInstituicao = g.getPosicaoClinicaByNif(nif);
        if(posicaoInstituicao != -1){
            Dentista d = Converter.dentistaDTO2Dentista(dentistaDTO);
            g.adicionarPessoa(d, posicaoInstituicao);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF inserido!");
    }

    public static void adicionarCliente(ClienteDTO cliente, long nif) throws NifNaoEncontradoException{
        Gestao g = Dados.carregarDados();
        int posicaoInstituicao = g.getPosicaoClinicaByNif(nif);
        if(posicaoInstituicao != -1){
            Cliente c = Converter.clienteParcialDTO2Cliente(cliente);
            g.adicionarPessoa(c, posicaoInstituicao);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF inserido!");
    }

    public static void eliminarPessoa(long nif, long cartaoCartaoCidadao) throws CartaoCidadaoNaoEncontradoException, NifNaoEncontradoException{
        Gestao g = Dados.carregarDados();
        int posicaoInstituicao = g.getPosicaoClinicaByNif(nif);
        if(posicaoInstituicao != -1){
            if(g.getClinicaByNif(nif).getPessoaByCC(cartaoCartaoCidadao) != null){
                g.eliminarPessoa(cartaoCartaoCidadao, posicaoInstituicao);
                Dados.guardarDados(g);
            }
            else
                throw new CartaoCidadaoNaoEncontradoException("Não existe na clínica nenhuma pessoa registada com esse cartão de cidadão!");
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF inserido!");
    }

    public static void alterarDentista(long nif, long cc, DentistaDTO dentistaDTO){
        Gestao g = Dados.carregarDados();
        Dentista dentistaRegistado = (Dentista) g.getClinicaByNif(nif).getPessoaByCC(cc);
        Dentista dentista = Converter.dentistaDTO2Dentista(dentistaDTO);
        dentistaRegistado.setNumeroCedula(dentista.getNumeroCedula());
        dentistaRegistado.setNome(dentista.getNome());
        dentistaRegistado.setCartaoCidadao(dentista.getCartaoCidadao());
        dentistaRegistado.setDataNascimento(dentista.getDataNascimento());
        Dados.guardarDados(g);
    }

    public static void alterarCliente(long nif, long cartaoCidadao, ClienteDTO cliente){
        Gestao g = Dados.carregarDados();
        Clinica clinica = g.getClinicaByNif(nif);
        if(g.getPosicaoClinicaByNif(nif) != -1){
            List<Pessoa> pessoasRegistadas = clinica.getPessoas();
            Clinica cl = g.getClinicaByNif(nif);
            if(g.getClinicaByNif(nif).getPessoaByCC(cartaoCidadao) != null){
                if(cl.getPessoaByCC(cartaoCidadao) instanceof Cliente){
                    for(int i = 0; i < pessoasRegistadas.size(); i++){
                        if(cartaoCidadao == pessoasRegistadas.get(i).getCartaoCidadao()){
                            g.eliminarPessoa(cartaoCidadao, g.getPosicaoClinicaByNif(nif));
                            Cliente c = Converter.clienteParcialDTO2Cliente(cliente);
                            g.adicionarPessoa(c, g.getPosicaoClinicaByNif(nif));
                        }
                    }
                    Dados.guardarDados(g);
                }
                else
                    throw new TipoPessoaInvalidoException("O cartão de cidadão introduzido não pertence a um cliente!");
            }
            else
                throw new CartaoCidadaoNaoEncontradoException("Não existe na clínica nenhuma pessoa registada com esse cartão de cidadão!");
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaDentistaParcialDTO getSoDentistas(long nif){
        Gestao g = Dados.carregarDados();
        List<Pessoa> pessoas = g.getClinicaByNif(nif).getPessoas();
        ArrayList<Dentista> dentistas = new ArrayList<>();
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i) instanceof Dentista)
                dentistas.add((Dentista) pessoas.get(i));
        }
        return Converter.listaDentista2ListaDentistaParcialDTO(dentistas);
    }

    public static DentistaDTO getDentista(long nif, long cc){
        Gestao g = Dados.carregarDados();
        Dentista d = null;
        List<Pessoa> pessoas = g.getClinicaByNif(nif).getPessoas();
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i) instanceof Dentista && pessoas.get(i).getCartaoCidadao() == cc){
                d = (Dentista) pessoas.get(i);
            }
        }
        DentistaDTO dentista = Converter.dentista2DentistaDTO(d);

        return dentista;
    }

    public static ListaClienteParcialDTO getSoClientes(long nif){
        Gestao g = Dados.carregarDados();
        List<Pessoa> pessoas = g.getClinicaByNif(nif).getPessoas();
        ArrayList<Cliente> clientes = new ArrayList<>();
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i) instanceof Cliente)
                clientes.add((Cliente) pessoas.get(i));
        }
        return Converter.listaCliente2ListaClienteParcialDTO(clientes);
    }

    public static ClienteDTO getCliente(long nif, long cc){
        Gestao g = Dados.carregarDados();
        Cliente c = null;
        List<Pessoa> pessoas = g.getClinicaByNif(nif).getPessoas();
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i) instanceof Cliente && pessoas.get(i).getCartaoCidadao() == cc){
                c = (Cliente) pessoas.get(i);
            }
        }
        ClienteDTO clienteDTO = Converter.cliente2ClienteDTO(c);

        return clienteDTO;
    }
}
