package pt.isep.clinic.exception;

public class AvaliacaoJaExistenteException extends RuntimeException{
    public AvaliacaoJaExistenteException(String s){ super(s);}
}
