package pt.isep.clinic.service;
import pt.isep.clinic.dao.Dados;
import pt.isep.clinic.dto.Converter;
import pt.isep.clinic.dto.ListaServicoParcialDTO;
import pt.isep.clinic.dto.ServicoDTO;
import pt.isep.clinic.exception.NifNaoEncontradoException;
import pt.isep.clinic.model.*;
import java.text.ParseException;
import java.util.List;

public class ConsultasService {

    public static void adicionarConsulta(ServicoDTO servico, long nif) throws ParseException {
        Gestao g = Dados.carregarDados();
        int posicaoCLinica = g.getPosicaoClinicaByNif(nif);
        if(posicaoCLinica != -1){
            Servico s = Converter.servicoParcialDTO2Servico(servico);
            g.adicionarConsulta(s, nif);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ListaServicoParcialDTO getConsultas(long nif){
        Gestao g = Dados.carregarDados();
        int posicaoClinica = g.getPosicaoClinicaByNif(nif);
        if(posicaoClinica != -1){
            return Converter.listaServico2ListaServicoParcialDTO(g.getClinicaByNif(nif).getServicos());
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static void eliminarConsulta(int codigoConsulta, long nif){
        Gestao g = Dados.carregarDados();
        int posicaoClinica = g.getPosicaoClinicaByNif(nif);
        if(posicaoClinica != -1){
            g.eliminarConsulta(codigoConsulta, nif);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static void alterarConsulta(int codigoConsulta, ServicoDTO servico, long nif) throws ParseException{
        Gestao g = Dados.carregarDados();
        int posicaoClinica = g.getPosicaoClinicaByNif(nif);
        if(posicaoClinica != -1){
            g.eliminarConsulta(codigoConsulta, nif);
            g.adicionarConsulta(Converter.servicoParcialDTO2Servico(servico), nif);
            Dados.guardarDados(g);
        }
        else
            throw new NifNaoEncontradoException("Não existe nenhuma clínica com o NIF introduzido!");
    }

    public static ServicoDTO getServico(long nif, int codigo){
        Gestao g = Dados.carregarDados();
        Servico servico;
        List<Servico> s = g.getClinicaByNif(nif).getServicos();
        for(int i = 0; i < s.size(); i++){
            if(s.get(i).getCodigoConsulta() == codigo)
            {
                servico = s.get(i);
                ServicoDTO servicoDTO = Converter.servico2ServicoDTO(servico);
                return servicoDTO;
            }
        }
        return null;
    }
}
