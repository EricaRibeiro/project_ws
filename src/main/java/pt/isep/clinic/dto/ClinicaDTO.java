package pt.isep.clinic.dto;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JsonPropertyOrder({"nome", "nif", "data"})
@JacksonXmlRootElement(localName = "clinica")
public class ClinicaDTO {
    @JacksonXmlProperty(localName = "nome")
    private String nome;
    @JacksonXmlProperty(localName = "nif")
    private long nif;
    @JacksonXmlProperty(localName = "data")
    private DataDTO dataConstituicao;

    public ClinicaDTO(){
    }

    public ClinicaDTO(String nome, long nif, DataDTO dataConstituicao){
        this.nome = nome;
        this.nif = nif;
        this.dataConstituicao = dataConstituicao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
        this.nif = nif;
    }

    public DataDTO getDataConstituicao() {
        return dataConstituicao;
    }

    public void setDataConstituicao(DataDTO dataConstituicao) {
        this.dataConstituicao = dataConstituicao;
    }
}
