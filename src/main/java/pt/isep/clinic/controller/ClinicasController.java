package pt.isep.clinic.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isep.clinic.dto.ClinicaDTO;
import pt.isep.clinic.dto.ErroDTO;
import pt.isep.clinic.dto.ListaClinicaParcialDTO;
import pt.isep.clinic.service.ClinicasService;

@RestController
@RequestMapping("/api")
public class ClinicasController {

    @RequestMapping(value = "/clinica",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> addClinica(@RequestBody ClinicaDTO clinicaDTO) {
        try{
            ClinicasService.adicionarClinica(clinicaDTO);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinicas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getClinicas() {
        try {
            ListaClinicaParcialDTO listaClinicaParcialDTO = ClinicasService.getClinicas();
            return new ResponseEntity<>(listaClinicaParcialDTO, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> deleteClinica(@PathVariable("nif") long nif) {
        try{
            ClinicasService.eliminarClinica(nif);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinicas/{nif}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_XML_VALUE,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> updateClinica(@PathVariable("nif") long nif, @RequestBody ClinicaDTO clinicaDTO) {
        try{
            ClinicasService.alterarClinica(nif, clinicaDTO);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/getClinica/{nif}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getClinica(@PathVariable("nif") long nif) {
        try{
            ClinicaDTO c = ClinicasService.getClinica(nif);
            return new ResponseEntity<>(c, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }
}
