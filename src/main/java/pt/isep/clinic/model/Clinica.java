package pt.isep.clinic.model;
import pt.isep.clinic.exception.*;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Clinica implements Serializable {
    private String nome;
    private Data dataConstituicao;
    private long nif;
    private List<Pessoa> pessoas = new ArrayList<>();
    private List<Servico> servicos = new ArrayList<>();
    private List<Avaliacao> avaliacoes = new ArrayList<>();

    public Clinica(String nome, Data dataConstituicao, long nif){
        this.nome = nome;
        this.dataConstituicao = dataConstituicao;
        this.nif = nif;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Data getDataConstituicao() {
        return dataConstituicao;
    }

    public void setDataConstituicao(Data dataConstituicao) {
        this.dataConstituicao = dataConstituicao;
    }

    public long getNif() {
        return nif;
    }

    public void setNif(long nif) {
        this.nif = nif;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public List<Servico> getServicos() {
        return servicos;
    }

    public void setServicos(List<Servico> servicos) {
        this.servicos = servicos;
    }

    public List<Avaliacao> getAvaliacoes() {
        return avaliacoes;
    }

    public void setAvaliacoes(List<Avaliacao> avaliacoes) {
        this.avaliacoes = avaliacoes;
    }

    public void addPessoa(Pessoa p){
        ArrayList<Dentista> dentistasRegistados = new ArrayList<>();
        for(int i = 0; i < pessoas.size(); i++) {
            if (p.getCartaoCidadao() == pessoas.get(i).getCartaoCidadao() || (p instanceof Dentista && pessoas.get(i) instanceof Dentista && ((Dentista) p).getNumeroCedula() == ((Dentista) pessoas.get(i)).getNumeroCedula()))
                throw new PessoaJaExistenteNaClinicaException("A pessoa já se encontra registada na clínica em questão!");
            else {
                if (!Validacoes.validarTamanhoNumeros(p.getCartaoCidadao()) || (p instanceof Dentista && !Validacoes.validarTamanhoNumeros(((Dentista) p).getNumeroCedula())) || (p instanceof Cliente && !Validacoes.validarTamanhoNumeros(((Cliente) p).getIdentificacaoDentista()))){
                    throw new NumeroInvalidoException("O número de cédula, número de identificação do dentista e o número de cartão de cidadão devem ser compostos por 9 dígitos!");
                }
            }
        }
        for(int j = 0; j < pessoas.size(); j++) {
            if(pessoas.get(j) instanceof Dentista)
                dentistasRegistados.add((Dentista) pessoas.get(j));
        }
        if(p instanceof Cliente){
            boolean flag = false;
            for(int h = 0; h < dentistasRegistados.size(); h++){
                if(dentistasRegistados.get(h).getCartaoCidadao() == ((Cliente) p).getIdentificacaoDentista())
                    flag = true;
            }
            if(!flag)
                throw new DentistaInexistenteException("Não existe nenhum dentista com o cartão de cidadão introduzido!");
        }
        pessoas.add(p);
    }

    public Pessoa getPessoaByCC(long cc){
        for(int i = 0; i < pessoas.size(); i++){
            if(cc == pessoas.get(i).getCartaoCidadao())
                return pessoas.get(i);
        }
        return null;
    }

    public void deletePessoa(long cartaoCidadao){
        for(int i = 0; i < pessoas.size(); i++){
            if(pessoas.get(i).getCartaoCidadao() == cartaoCidadao)
            {
                Pessoa p = pessoas.get(i);
                if(p instanceof Dentista){
                    for(int j = 0; j < pessoas.size(); j++){
                        if(pessoas.get(j) instanceof Cliente && p.getCartaoCidadao() == ((Cliente) pessoas.get(j)).getIdentificacaoDentista()){
                            throw new ImpossivelEliminarDentistaException("Proceda primeiro à alteração dos utentes que têm como numeroIdentificacaoDentista " + p.getCartaoCidadao());
                        }
                    }
                }
                pessoas.remove(p);
            }
        }
    }

    public void adicionarConsulta(Servico s) throws ParseException {
        boolean flag = true, flag2 = true;
        if(getPessoaByCC(s.getIdentificacaoCliente()) != null){
            for (int j = 0; j < servicos.size(); j++) {
                if (servicos.get(j).getCodigoConsulta() == s.getCodigoConsulta())
                    throw new CodigoConsultaDuplicadoException("Já existe uma consulta com o código de consulta introduzido!");
                else {
                    if(!(getPessoaByCC(s.getIdentificacaoCliente()) instanceof Cliente))
                        throw new TipoPessoaInvalidoException("O cartão de cidadão introduzido não corresponde ao de um cliente!");
                    else{
                        String a = (s.getHoraRealizacao().getHora()) + Integer.toString(s.getHoraRealizacao().getMinutos());
                        String b = (servicos.get(j).getHoraRealizacao().getHora()) + Integer.toString(servicos.get(j).getHoraRealizacao().getMinutos());
                        if(a.equals(b) && s.getDataRealizacao().toString().equals(servicos.get(j).getDataRealizacao().toString()))
                            flag = false;
                        else {
                            if(s.getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("manutençãodeaparelho")){
                                if(servicos.get(j).getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("colocaçãodeaparelho") && servicos.get(j).getIdentificacaoCliente() == s.getIdentificacaoCliente())
                                    flag2 = true;
                                else
                                    flag2 = false;
                            }
                            else {
                                if((s.getTipoServico().trim().toLowerCase(Locale.ROOT).replace(" ", "").equals("limpeza") || s.getTipoServico().trim().toLowerCase(Locale.ROOT).replace(" ", "").equals("colocaçãodeaparelho")) && servicos.get(j).getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("urgência") && !Data.validarDataTipoServicoConsulta(s.getDataRealizacao(), servicos.get(j).getDataRealizacao()) && s.getIdentificacaoCliente() == servicos.get(j).getIdentificacaoCliente())
                                    throw new ImpossivelAdicionarConsultaException("Como o cliente teve uma consulta do tipo \"urgência\" nos últimos 30 dias, não é possível agendar a limpeza/colocação do aparelho!");
                                else if(servicos.get(j).getTipoServico().trim().toLowerCase(Locale.ROOT).replace(" ", "").equals("colocaçãodeaparelho") && servicos.get(j).getIdentificacaoCliente() == s.getIdentificacaoCliente() && s.getTipoServico().trim().toLowerCase(Locale.ROOT).replace(" ", "").equals("colocaçãodeaparelho"))
                                    throw new ImpossivelAdicionarConsultaException("O cliente em questão já colocou o aparelho!");
                            }
                        }
                    }
                }
            }
            if(!flag)
                throw new HoraInvalidaException("Já existe uma consulta marcada para a hora introduzida!");
            else if(!flag2)
                throw new ImpossivelAdicionarConsultaException("O cliente ainda não colocou o aparelho! Impossível marcar/registar \"manutenção do aparelho\"");
            servicos.add(s);
        }
        else
            throw new CartaoCidadaoNaoEncontradoException("Não existe na clínica nenhum cliente com o cartão de cidadão introduzido!");
    }

    public void eliminarConsulta(int codigoConsulta){
        boolean flag = false;
        for(int i = 0; i < servicos.size(); i++){
            if(servicos.get(i).getCodigoConsulta() == codigoConsulta)
            {
                flag = true;
                servicos.remove(servicos.get(i));
            }
        }
        if(!flag)
            throw new CodigoConsultaInvalidoException("Não existe nenhuma consulta com o código de consulta inserido!");
    }

    public double getTaxa(){
        int totalConsultas = servicos.size();
        int urgencias = 0;
        for(int i = 0; i < servicos.size(); i++){
            if(servicos.get(i).getTipoServico().toLowerCase(Locale.ROOT).trim().replace(" ", "").equals("urgência"))
                urgencias += 1;
        }
        double taxaUrgencia = (urgencias / (totalConsultas * 1.0)) * 100;

        return taxaUrgencia;
    }

    public void addAvaliacao(Avaliacao avaliacao){
        for(int i = 0; i < avaliacoes.size(); i++){
            if(avaliacao.getIdentificacaoCliente() == avaliacoes.get(i).getIdentificacaoCliente())
                throw new AvaliacaoJaExistenteException("Esse cliente já avaliou a clínica! Um cliente só pode avaliar a clínica uma vez!");
        }
        avaliacoes.add(avaliacao);
    }

    public int popularidadeClinica(){
        int count = 0;
        for(int i = 0; i < avaliacoes.size(); i++){
            count += avaliacoes.get(i).getAvaliacao();
        }

        return count;
    }
}
