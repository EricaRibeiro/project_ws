package pt.isep.clinic.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.isep.clinic.dto.ErroDTO;
import pt.isep.clinic.dto.ListaServicoParcialDTO;
import pt.isep.clinic.dto.ServicoDTO;
import pt.isep.clinic.service.ConsultasService;

@RestController
@RequestMapping("/api")
public class ConsultasController {

    @RequestMapping(value = "/clinica/{nif}/consulta",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> addConsulta(@PathVariable("nif") long nif, @RequestBody ServicoDTO servicoDTO) {
        try{
            ConsultasService.adicionarConsulta(servicoDTO, nif);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/consultas",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getConsultas(@PathVariable("nif") long nif) {
        try{
            ListaServicoParcialDTO listaServicoParcialDTO = ConsultasService.getConsultas(nif);
            return new ResponseEntity<>(listaServicoParcialDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/consultas/{codigoConsulta}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> deleteConsulta(@PathVariable("nif") long nif, @PathVariable("codigoConsulta") int codigoConsulta) {
        try{
            ConsultasService.eliminarConsulta(codigoConsulta, nif);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "consultas/{codigoConsulta}/clinica/{nif}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_XML_VALUE,
            consumes = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> updateConsulta(@PathVariable("codigoConsulta") int codigoConsulta, @PathVariable("nif") long nif, @RequestBody ServicoDTO servicoDTO) {
        try{
            ConsultasService.alterarConsulta(codigoConsulta, servicoDTO, nif);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }

    @RequestMapping(value = "/clinica/{nif}/consulta/{codigoConsulta}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<Object> getConsulta(@PathVariable("nif") long nif, @PathVariable("codigoConsulta") int cod) {
        try{
            ServicoDTO servicoDTO = ConsultasService.getServico(nif, cod);
            return new ResponseEntity<>(servicoDTO, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(new ErroDTO(e), HttpStatus.CONFLICT);
        }
    }
}
