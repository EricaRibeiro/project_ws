package pt.isep.clinic.model;
import java.io.Serializable;

public class Pessoa implements Serializable {
    private String nome;
    private long cartaoCidadao;
    private Data dataNascimento;

    public Pessoa(String nome, long cartaoCidadao, Data dataNascimento){
        this.nome = nome;
        this.cartaoCidadao = cartaoCidadao;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public long getCartaoCidadao() {
        return cartaoCidadao;
    }

    public void setCartaoCidadao(long cartaoCidadao) {
        this.cartaoCidadao = cartaoCidadao;
    }

    public Data getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Data dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
