package pt.isep.clinic.dto;
import pt.isep.clinic.model.*;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    public static Data dataDTO2Data(DataDTO data){
        return new Data(data.getDia(), data.getMes(), data.getAno());
    }

    public static DataDTO data2DataDTO(Data data){
        return new DataDTO(data.getDia(), data.getMes(), data.getAno());
    }

    public static Hora horaDTO2Hora(HoraDTO hora){
        return new Hora(hora.getHora(), hora.getMinutos());
    }

    public static HoraDTO hora2HoraDTO(Hora hora){
        return new HoraDTO(hora.getHora(), hora.getMinutos());
    }

    public static Dentista dentistaDTO2Dentista(DentistaDTO d){
        Data data = dataDTO2Data(d.getDataNascimento());
        return new Dentista(d.getNome(), d.getCartaoCidadao(), data, d.getNumeroCedula());
    }

    public static Cliente clienteParcialDTO2Cliente(ClienteDTO c){
        Data data = dataDTO2Data(c.getDataNascimento());
        return new Cliente(c.getNome(), c.getCartaoCidadao(), data, c.getIdentificacaoDentista());
    }

    public static Clinica clinicaDTO2Clinica(ClinicaDTO clinica){
        Data dataConstituicao = dataDTO2Data(clinica.getDataConstituicao());
        return new Clinica(clinica.getNome(), dataConstituicao, clinica.getNif());
    }

    public static Servico servicoParcialDTO2Servico(ServicoDTO s){
        Data d = dataDTO2Data(s.getData());
        Hora h = horaDTO2Hora(s.getHora());
        return new Servico(s.getTipoServico(), s.getPreco(), d, h, s.getIdentificacaoCLiente(), s.getCodigoConsulta());
    }

    public static Avaliacao avaliacaoDTO2Avaliacao(AvaliacaoDTO avaliacao){
        return new Avaliacao(avaliacao.getAvaliacao(), avaliacao.getIdentificacaoCliente());
    }

    public static ClinicaParcialDTO clinica2ClinicaParcialDTO(Clinica c){
        ClinicaParcialDTO clinicaParcialDTO = new ClinicaParcialDTO();
        clinicaParcialDTO.setNif(c.getNif());
        clinicaParcialDTO.setNome(c.getNome());

        return clinicaParcialDTO;
    }

    public static ListaClinicaParcialDTO listaClinica2ListaClinicaParcialDTO(ArrayList<Clinica> clinicas) throws NullPointerException {
        ArrayList<ClinicaParcialDTO> clinicasDTO = new ArrayList<>();
        for (Clinica clinica : clinicas) {
            try {
                ClinicaParcialDTO clinicaParcialDTO = clinica2ClinicaParcialDTO(clinica);
                clinicasDTO.add(clinicaParcialDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaClinicaParcialDTO listaClinicaParcialDTO = new ListaClinicaParcialDTO();
        listaClinicaParcialDTO.setClinicas(clinicasDTO);
        return listaClinicaParcialDTO;
    }

    public static ClinicaDTO clinica2ClinicaDTO(Clinica c){
        ClinicaDTO clinicaDTO = new ClinicaDTO();
        clinicaDTO.setNif(c.getNif());
        clinicaDTO.setNome(c.getNome());
        DataDTO d = data2DataDTO(c.getDataConstituicao());
        clinicaDTO.setDataConstituicao(d);

        return clinicaDTO;
    }

    public static ListaDentistaParcialDTO listaDentista2ListaDentistaParcialDTO(ArrayList<Dentista> dentistas) throws NullPointerException {
        ArrayList<DentistaParcialDTO> dentistasDTO = new ArrayList<>();
        for (Dentista dentista : dentistas) {
            try {
                DentistaParcialDTO dentistaParcialDTO = dentista2DentistaParcialDTO(dentista);
                dentistasDTO.add(dentistaParcialDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaDentistaParcialDTO listaDentistaParcialDTO = new ListaDentistaParcialDTO();
        listaDentistaParcialDTO.setDentistas(dentistasDTO);
        return listaDentistaParcialDTO;
    }

    public static DentistaParcialDTO dentista2DentistaParcialDTO(Dentista d){
        DentistaParcialDTO dentistaParcialDTO = new DentistaParcialDTO();
        dentistaParcialDTO.setNumeroCedula(d.getNumeroCedula());
        dentistaParcialDTO.setNome(d.getNome());
        dentistaParcialDTO.setCartaoCidadao(d.getCartaoCidadao());

        return dentistaParcialDTO;
    }

    public static DentistaDTO dentista2DentistaDTO(Dentista d){
        DentistaDTO dentista = new DentistaDTO();
        dentista.setNome(d.getNome());
        dentista.setCartaoCidadao(d.getCartaoCidadao());
        dentista.setNumeroCedula(d.getNumeroCedula());
        DataDTO data = data2DataDTO(d.getDataNascimento());
        dentista.setDataNascimento(data);

        return dentista;
    }

    public static ListaClienteParcialDTO listaCliente2ListaClienteParcialDTO(ArrayList<Cliente> clientes) throws NullPointerException {
        ArrayList<ClienteParcialDTO> clientesDTO = new ArrayList<>();
        for (Cliente cliente : clientes) {
            try {
                ClienteParcialDTO clienteParcialDTO = cliente2ClienteParcialDTO(cliente);
                clientesDTO.add(clienteParcialDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaClienteParcialDTO listaClienteParcialDTO = new ListaClienteParcialDTO();
        listaClienteParcialDTO.setClientes(clientesDTO);
        return listaClienteParcialDTO;
    }

    public static ClienteParcialDTO cliente2ClienteParcialDTO(Cliente c){
        ClienteParcialDTO clienteParcialDTO = new ClienteParcialDTO();
        clienteParcialDTO.setIdentificacaoDentista(c.getIdentificacaoDentista());
        clienteParcialDTO.setNome(c.getNome());
        clienteParcialDTO.setCartaoCidadao(c.getCartaoCidadao());

        return clienteParcialDTO;
    }

    public static ClienteDTO cliente2ClienteDTO(Cliente c){
        ClienteDTO cliente = new ClienteDTO();
        cliente.setNome(c.getNome());
        cliente.setCartaoCidadao(c.getCartaoCidadao());
        cliente.setIdentificacaoDentista(c.getIdentificacaoDentista());
        DataDTO data = data2DataDTO(c.getDataNascimento());
        cliente.setDataNascimento(data);

        return cliente;
    }

    public static ListaServicoParcialDTO listaServico2ListaServicoParcialDTO(List<Servico> servicos) throws NullPointerException {
        ArrayList<ServicoParcialDTO> servicosDTO = new ArrayList<>();
        for (Servico servico : servicos) {
            try {
                ServicoParcialDTO servicoParcialDTO = servico2ServicoParcialDTO(servico);
                servicosDTO.add(servicoParcialDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaServicoParcialDTO listaServicoParcialDTO = new ListaServicoParcialDTO();
        listaServicoParcialDTO.setServicos(servicosDTO);
        return listaServicoParcialDTO;
    }

    public static ServicoParcialDTO servico2ServicoParcialDTO(Servico s){
        ServicoParcialDTO servicoParcialDTO = new ServicoParcialDTO();
        servicoParcialDTO.setTipoServico(s.getTipoServico());
        servicoParcialDTO.setCodigoConsulta(s.getCodigoConsulta());
        DataDTO d = data2DataDTO(s.getDataRealizacao());
        servicoParcialDTO.setData(d);
        servicoParcialDTO.setIdentificacaoCLiente(s.getIdentificacaoCliente());
        HoraDTO h = hora2HoraDTO(s.getHoraRealizacao());
        servicoParcialDTO.setHora(h);

        return servicoParcialDTO;
    }

    public static ListaTaxaUrgenciaDTO listaTaxaUrgencia2ListaTaxaUrgenciaDTO(ArrayList<TaxaUrgencia> taxas) throws NullPointerException {
        ArrayList<TaxaUrgenciaDTO> taxaDTO = new ArrayList<>();
        for (TaxaUrgencia taxa : taxas) {
            try {
                TaxaUrgenciaDTO taxaUrgenciaDTO = taxaUrgencia2TaxaUrgenciaDTO(taxa);
                taxaDTO.add(taxaUrgenciaDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaTaxaUrgenciaDTO listaTaxaUrgenciaDTO = new ListaTaxaUrgenciaDTO();
        listaTaxaUrgenciaDTO.setTaxas(taxaDTO);
        return listaTaxaUrgenciaDTO;
    }

    public static TaxaUrgenciaDTO taxaUrgencia2TaxaUrgenciaDTO(TaxaUrgencia t){
        TaxaUrgenciaDTO taxaUrgenciaDTO = new TaxaUrgenciaDTO();
        taxaUrgenciaDTO.setTaxaUrgencia(t.getTaxaUrgencia());
        taxaUrgenciaDTO.setNomeClinica(t.getNomeClinica());

        return taxaUrgenciaDTO;
    }

    public static ListaUrgenciasMesDTO listaUrgenciasMes2ListaUrgenciasMesDTO(ArrayList<UrgenciasMes> urgencias) throws NullPointerException {
        ArrayList<UrgenciasMesDTO> urgenciaDTO = new ArrayList<>();
        for (UrgenciasMes urgencia : urgencias) {
            try {
                UrgenciasMesDTO urgenciasMesDTO = urgenciasMes2UrgenciasMesDTO(urgencia);
                urgenciaDTO.add(urgenciasMesDTO);
            } catch (NullPointerException e) {
                //does nothing. Actually, nothing is added to arraylist
            }
        }
        ListaUrgenciasMesDTO listaUrgenciasMesDTO = new ListaUrgenciasMesDTO();
        listaUrgenciasMesDTO.setUrgencias(urgenciaDTO);
        return listaUrgenciasMesDTO;
    }

    public static UrgenciasMesDTO urgenciasMes2UrgenciasMesDTO(UrgenciasMes u){
        UrgenciasMesDTO urgenciasMesDTO = new UrgenciasMesDTO();
        urgenciasMesDTO.setNumeroUrgencias(u.getNumeroUrgencias());
        urgenciasMesDTO.setNomeClinica(u.getNomeClinica());

        return urgenciasMesDTO;
    }

    public static ServicoDTO servico2ServicoDTO(Servico s){
        ServicoDTO servicoDTO = new ServicoDTO();
        servicoDTO.setTipoServico(s.getTipoServico());
        servicoDTO.setCodigoConsulta(s.getCodigoConsulta());
        servicoDTO.setIdentificacaoCLiente(s.getIdentificacaoCliente());
        DataDTO data = data2DataDTO(s.getDataRealizacao());
        servicoDTO.setData(data);
        HoraDTO hora = hora2HoraDTO(s.getHoraRealizacao());
        servicoDTO.setHora(hora);
        servicoDTO.setPreco(s.getPreco());

        return servicoDTO;
    }

}
