package pt.isep.clinic.exception;

public class ImpossivelEliminarDentistaException extends RuntimeException{
    public ImpossivelEliminarDentistaException(String s) {
        super(s);
    }
}
