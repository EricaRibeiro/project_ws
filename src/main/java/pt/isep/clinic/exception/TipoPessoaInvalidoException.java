package pt.isep.clinic.exception;

public class TipoPessoaInvalidoException extends RuntimeException{
    public TipoPessoaInvalidoException(String s) {
        super(s);
    }
}
